function [h, G] = system2graph(blocks, inputs, outputs, varargin)
%system2graph displays system as a graph. 
% Purpose of this function is to allow user to check input to connect()
% command used mainly in robust control.
% system2graph(blocks, inputs, outputs [, markerSize])
% arguments:
%   - blocks       - cell array of build blocks
%   - inputs       - cell array of names of inputs of the system
%   - outputs      - cell array of names of outputs of the system
%   - [markerSize] - number of size of block markers (optional)
% returned variables:
%   - h            - created plot
%   - G            - created graph definition
% example:
%   for connect(P, K, sum1, {'w'}, {'y'}) 
%   use system2graph({P, K, sum1}, {'w'}, {'y'}, 10);

    i = 1;
    while i<=length(varargin)
        switch(varargin{i})
            case 'MarkerSize' 
                if ~isnumeric(varargin{i+1})
                    error(strcat('Wrong MarkerSize: ', varargin{i+1}));
                    return
                end
                markerSize = varargin{i+1};
                i = i+2;
            case 'Nmeas'
                if ~isnumeric(varargin{i+1})
                    error(strcat('Wrong Nmeas: ', varargin{i+1}));
                    return
                end
                nmeas = varargin{i+1};
                i = i+2;
            case 'Ncont'
                if ~isnumeric(varargin{i+1})
                    error(strcat('Wrong Ncont: ', varargin{i+1}));
                    return
                end
                ncont = varargin{i+1};
                i = i+2;
            case 'SimulateController'
                SimulateController = true;
                i = i+1;
            otherwise
                error(strcat('Unknown argument ', varargin{i}));
                return
        end
    end

    if ~exist('markerSize')
        markerSize = 4;
    end
    if ~exist('nmeas')
        nmeas = 0;
    end
    if ~exist('ncont')
        ncont = 0;
    end
    if ~exist('SimulateController')
        SimulateController = false;
    end
    
    blockNames = {};
    connectionNames = {};
    G = digraph([], []);
    for block = blocks
        blockNames{end+1} = block{1}.name;
        for input = block{1}.u
            G = addedge(G, input, block{1}.name);
            if isempty(block{1}.name)
                error('Not all names are defined, quitting');
                return
            end
            for i = 1:length(input)
                connectionNames{end+1} = input{i};
            end
        end
        for output = block{1}.y
            G = addedge(G, block{1}.name, output);
            for i = 1:length(output)
                connectionNames{end+1} = output{i};
            end
        end
    end
    
    if SimulateController
        G = addedge(G, 'Controller',inputs(end-(ncont-1):end));
        G = addedge(G, outputs(end-(nmeas-1):end), 'Controller');
    end
    
    figure
    h = plot(G);
    if SimulateController
        layout(h,'layered','Direction','right','Sources',inputs(1:end-ncont), 'Sinks', outputs(1:end-nmeas))
    else
        layout(h,'layered','Direction','right','Sources',inputs, 'Sinks', outputs)
    end
    highlight(h,unique(blockNames), 'MarkerSize', markerSize)
    highlight(h,unique(connectionNames), 'Marker', 'none') 
    
    for connection=unique(setdiff(connectionNames, [inputs, outputs]))
        if (length(successors(G, connection{1})) < 1) || (length(predecessors(G, connection{1})) < 1)
            warning(strjoin([{'connection'}, connection, {'is not used'}]))
            highlight(h,connection, 'Marker', 'x', 'NodeColor', 'r', 'MarkerSize', 6) 
        end
    end
    
    for node = [outputs(end-(nmeas-1):end) inputs(end-(ncont-1):end)]
        highlight(h, node, 'Marker', '>', 'NodeColor', [0.4940 0.1840 0.5560], 'MarkerSize', markerSize) 
    end
    if SimulateController
        highlight(h, 'Controller', 'NodeColor', [0.1 0.6 0.1], 'MarkerSize', markerSize) 
    end
end

%% Testing code
% P = rss(1,1,1);
% K = rss(1,1,1);
% sum1 = sumblk('e = y-w');
% 
% P.name = 'P';
% P.u = 'x';
% P.y = 'y';
% 
% K.name = 'K';
% K.u = 'e';
% K.y = 'x';
% 
% sum1.name = '+';
% 
% system2graph({P,K,sum1}, {'w'}, {'y'}, 7);
