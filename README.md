# system2graph

Matlab function for displaying systems in robust control toolbox

Purpose of this function is to allow user to check input to connect()
command used mainly in robust control.

arguments:
  - blocks       - cell array of build blocks
  - inputs       - cell array of names of inputs of the system
  - outputs      - cell array of names of outputs of the system
  - [MarkerSize] - number of size of block markers (optional)
  - [Nmeas]      - inputs into controller being designed (optional)
  - [Ncon]       - outputs from controller being designed (optional)

returned variables:
  - h            - created plot
  - G            - created graph definition

example output:
(read TestingScript for more information)
![img](img/exampleOutput.PNG)

## Installation
Run `install.m` script. Make sure to use "Change Folder" option if you are not in directory, where the script is.

## Uninstallation
Run `uninstall.m` script. Make sure to use "Change Folder" option if you are not in directory, where the script is.