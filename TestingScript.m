P = rss(3,3,3);
Wsomega = rss(1,1,1);
Wsa = rss(1,1,1);
Wsf = Wsa;
Wtomega = rss(1,1,1);
Wta = rss(1,1,1);
Wtf = Wta;

P.InputName = {'uf', 'ua', 'Tload'};
P.OutputName = {'if', 'ia', 'omega'};
P.StateName = {'uf', 'ua', 'Tload'};
P.name = "P";

Wsomega.u = 'eomega';
Wsomega.y = 'ysomega';
Wsomega.name = 'Wsomega';

Wsf.u = 'ef';
Wsf.y = 'ysf';
Wsf.name = 'Wsf';

Wsa.u = 'ea';
Wsa.y = 'ysa';
Wsa.name = 'Wsa';

Wtf.u = 'if';
Wtf.y = 'ytf';
Wtf.name = 'Wtf';

Wta.u = 'ia';
Wta.y = 'yta';
Wta.name = 'Wta';

Wtomega.u = 'omega';
Wtomega.y = 'ytomega';
Wtomega.name = 'Wtomega';

sum1 = sumblk('eomega=womega-omega');
sum1.name = 'sum1';
sum2 = sumblk('ef = wf - if');
sum2.name = 'sum2';
sum3 = sumblk('ea = wa - ia');
sum3.name = 'sum3';
sum4 = sumblk('ea = wa - ia');
sum4.name = 'sum4';

G = system2graph({P,...
                    Wsf,...
                    Wsa,...
                    Wtf,...
                    Wta,...
                    sum2,...
                    sum3},...
                    {'wf', 'wa', 'uf', 'ua'},... 
                    {'ysf', 'ysa', 'ytf', 'yta', 'ef', 'ea'}, 'Nmeas', 2, 'Ncont', 2, 'MarkerSize', 10);


%%
P = rss(1,1,1);
K = rss(1,1,1);
sum1 = sumblk('e = y-w');

P.name = 'P';
P.u = 'x';
P.y = 'y';

K.name = 'K';
K.u = 'e';
K.y = 'x';

sum1.name = '+';

system2graph({P,K,sum1}, {'w'}, {'y'}, 'MarkerSize', 10);
